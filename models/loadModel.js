const mongoose = require('mongoose');

const loadStates = [
  null,
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

const loadSchema = new mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    default: 'NEW',
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  },
  state: {
    type: String,
    default: null,
    enum: loadStates,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [{
    message: {
      type: String,
      default: null,
    },
    time: {
      type: Date,
      default: null,
    },
  }],
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.loadStates = loadStates;
module.exports.Load = mongoose.model('Load', loadSchema);
