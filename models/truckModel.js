const mongoose = require('mongoose');

const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    enum: truckTypes,
  },
  status: {
    type: String,
    enum: ['IS', 'OL'],
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.truckTypes = truckTypes;
module.exports.Truck = mongoose.model('Truck', truckSchema);
