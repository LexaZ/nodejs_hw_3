const {Load} = require('../models/loadModel');

const asyncWrapper = (callback) => {
  return (req, res, next) => {
    callback(req, res, next)
        .catch(next);
  };
};
const isDriver = () => {
  return (req, res, next) => {
    if (req.user.role !== 'DRIVER') {
      return res.status(403).json({message: 'Access denied!'});
    } else next();
  };
};
const isShipper = () => {
  return (req, res, next) => {
    if (req.user.role !== 'SHIPPER') {
      return res.status(403).json({message: 'Access denied!'});
    } else next();
  };
};
const isDriverOnLoad = () => {
  return async (req, res, next) => {
    const load = await Load.find({
      assigned_to: req.user._id,
      status: {$ne: 'SHIPPED'},
    });
    if (load.length !== 0) {
      return res.status(403).json({
        message: 'Driver is not able to do this while he is on a load!',
      });
    } else next();
  };
};

module.exports = {
  asyncWrapper,
  isDriver,
  isShipper,
  isDriverOnLoad,
};
