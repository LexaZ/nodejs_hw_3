const express = require('express');
const {asyncWrapper, isDriverOnLoad} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  showProfile,
  changePassword,
  deleteProfile,
} = require('../controllers/usersController');
const {changePasswordValidator} = require('./middlewares/validationMiddleware');

const usersRouter = new express.Router();

usersRouter.use(authMiddleware);
usersRouter.get('/me', showProfile);
usersRouter.patch('/me/password',
    isDriverOnLoad(),
    asyncWrapper(changePasswordValidator),
    asyncWrapper(changePassword),
);
usersRouter.delete('/me',
    isDriverOnLoad(),
    asyncWrapper(deleteProfile),
);

module.exports = usersRouter;
