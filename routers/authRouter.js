const express = require('express');
const {asyncWrapper} = require('./helpers');
const {registrationValidator} = require('./middlewares/validationMiddleware');
const {
  registration,
  login,
  forgotPassword,
} = require('../controllers/authController');

const authRouter = new express.Router();

authRouter.post('/register',
    asyncWrapper(registrationValidator),
    asyncWrapper(registration));
authRouter.post('/login', asyncWrapper(login));
authRouter.post('/forgot_password', asyncWrapper(forgotPassword));

module.exports = authRouter;
