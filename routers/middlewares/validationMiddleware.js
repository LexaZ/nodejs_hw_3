const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email({tlds: {allow: false}}),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    role: Joi.string()
        .valid('SHIPPER', 'DRIVER'),
  });
  await schema.validateAsync(req.body);
  next();
};

const changePasswordValidator = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });
  await schema.validateAsync(req.body);
  next();
};

module.exports = {
  registrationValidator,
  changePasswordValidator,
};
