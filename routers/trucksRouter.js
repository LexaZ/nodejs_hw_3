const express = require('express');
const {asyncWrapper, isDriver, isDriverOnLoad} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  postTruck,
  getTrucks,
  getTruck,
  putTruck,
  assignTruck,
  deleteTruck,
} = require('../controllers/trucksController');

const trucksRouter = new express.Router();

trucksRouter.use(authMiddleware);
trucksRouter.use(isDriver());
trucksRouter.post('/', asyncWrapper(postTruck));
trucksRouter.get('/', asyncWrapper(getTrucks));
trucksRouter.get('/:id', asyncWrapper(getTruck));
trucksRouter.put('/:id',
    isDriverOnLoad(),
    asyncWrapper(putTruck),
);
trucksRouter.post('/:id/assign', asyncWrapper(assignTruck));
trucksRouter.delete('/:id',
    isDriverOnLoad(),
    asyncWrapper(deleteTruck),
);

module.exports = trucksRouter;
