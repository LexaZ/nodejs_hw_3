const express = require('express');
const {asyncWrapper, isDriver, isShipper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  addLoadForUser,
  getUsersLoads,
  getUsersActiveLoad,
  nextLoadState,
  getUsersLoadById,
  updateUsersLoadById,
  deleteUsersLoadById,
  postUsersLoadById,
  getShippingInfoById,

} = require('../controllers/loadsController');

const loadsRouter = new express.Router();

loadsRouter.use(authMiddleware);
loadsRouter.post('/',
    isShipper(),
    asyncWrapper(addLoadForUser),
);
loadsRouter.get('/', asyncWrapper(getUsersLoads));
loadsRouter.get('/active',
    isDriver(),
    asyncWrapper(getUsersActiveLoad),
);
loadsRouter.patch('/active/state',
    isDriver(),
    asyncWrapper(nextLoadState),
);
loadsRouter.get('/:id', asyncWrapper(getUsersLoadById));
loadsRouter.put('/:id',
    isShipper(),
    asyncWrapper(updateUsersLoadById),
);
loadsRouter.delete('/:id',
    isShipper(),
    asyncWrapper(deleteUsersLoadById),
);
loadsRouter.post('/:id/post',
    isShipper(),
    asyncWrapper(postUsersLoadById),
);
loadsRouter.get('/:id/shipping_info',
    isShipper(),
    asyncWrapper(getShippingInfoById),
);

module.exports = loadsRouter;
