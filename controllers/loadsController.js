const {loadStates, Load} = require('../models/loadModel');
const {truckTypes, Truck} = require('../models/truckModel');

const addLoadForUser = async (req, res) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;
  const load = new Load({
    created_by: req.user._id,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });
  await load.save();
  res.status(200).json({message: 'Load created successfully'});
};

const getUsersLoads = async (req, res) => {
  const offset = parseInt(req.query.offset) || 0;
  let limit = parseInt(req.query.limit) || 10;
  limit = limit > 50 ? 50 : limit;

  if (req.query.status) {
    if (req.user.role === 'DRIVER') {
      const loads = await Load.find(
          {assigned_to: req.user._id, status: req.query.status},
          [],
          {
            skip: offset,
            limit: limit,
          },
      );
      return res.status(200).json({loads});
    } else if (req.user.role === 'SHIPPER') {
      const loads = await Load.find(
          {created_by: req.user._id, status: req.query.status},
          [],
          {
            skip: offset,
            limit: limit,
          },
      );
      return res.status(200).json({loads});
    };
  } else {
    if (req.user.role === 'DRIVER') {
      const loads = await Load.find(
          {assigned_to: req.user._id},
          [],
          {
            skip: offset,
            limit: limit,
          },
      );
      return res.status(200).json({loads});
    } else if (req.user.role === 'SHIPPER') {
      const loads = await Load.find(
          {created_by: req.user._id},
          [],
          {
            skip: offset,
            limit: limit,
          },
      );
      return res.status(200).json({loads});
    };
  }
};

const getUsersActiveLoad = async (req, res) => {
  const load = await Load.find({
    assigned_to: req.user._id,
    status: 'ASSIGNED',
  });
  res.status(200).json({load});
};

const nextLoadState = async (req, res) => {
  const load = await Load.findOne({
    assigned_to: req.user._id,
    status: 'ASSIGNED',
  });
  if (!load) {
    return res.status(200).json({message: 'You have no active load yet'});
  }
  const currentLoadStateIndex =
  await loadStates.findIndex((el) => el === load.state);
  load.state = loadStates[currentLoadStateIndex + 1];
  load.save();
  await load.updateOne({
    $push: {
      logs: {
        message: `Load state changed to '${load.state}'`,
        time: Date.now(),
      },
    },
  });
  if (load.state === 'Arrived to delivery') {
    await Truck.findOneAndUpdate(
        {assigned_to: req.user._id},
        {assigned_to: null, status: 'IS'},
    );
    await load.updateOne({
      status: 'SHIPPED',
      $push: {
        logs: {
          message: 'Load arrived to delivery',
          time: Date.now(),
        },
      },
    });
  }
  res.status(200).json({message: `Load state changed to '${load.state}'`});
};

const getUsersLoadById = async (req, res) => {
  const id = req.params.id;
  try {
    const load = await Load.findById(id);
    return res.status(200).json({load});
  } catch {
    return res.status(404).json({
      message: `A load with id '${id}' was not found`,
    });
  }
};

const updateUsersLoadById = async (req, res) => {
  const id = req.params.id;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;
  if (!name || !payload || !pickup_address ||
    !delivery_address || !dimensions) {
    return res.status(400).json({
      message: 'Please specify all required parameters',
    });
  }
  try {
    await Load.findByIdAndUpdate(id, {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    });
    res.status(200).json({message: 'Load details changed successfully'});
  } catch {
    return res.status(404).json({
      message: `A load with id '${id}' was not found`,
    });
  }
};

const deleteUsersLoadById = async (req, res) => {
  const id = req.params.id;
  try {
    await Load.findByIdAndDelete(id);
    res.status(200).json({message: 'Load deleted successfully'});
  } catch {
    return res.status(404).json({
      message: `A load with id '${id}' was not found`,
    });
  }
};

const postUsersLoadById = async (req, res) => {
  const id = req.params.id;
  try {
    await Load.findById(id);
  } catch {
    return res.status(404).json({
      message: `A load with id '${id}' was not found`,
    });
  }
  const load = await Load.findById(id);
  if (load.status !== 'NEW') {
    return res.status(405).json({
      message: `This action can only be applied to loads with the status 'NEW'`,
    });
  }
  await Load.findByIdAndUpdate(id, {status: 'POSTED'});

  const suitableTruckType =
    (load.dimensions.width < 300 && load.dimensions.length < 250 &&
        load.dimensions.height < 170 && load.payload < 1700) ? 'SPRINTER':
    (load.dimensions.width < 500 && load.dimensions.length < 250 &&
        load.dimensions.height < 170 && load.payload < 2500) ? 'SMALL STRAIGHT':
    (load.dimensions.width < 700 && load.dimensions.length < 350 &&
        load.dimensions.height < 200 && load.payload < 4000) ? 'LARGE STRAIGHT':
    null;

  if (!suitableTruckType) {
    await Load.findByIdAndUpdate(id, {
      status: 'NEW',
      logs: {message: 'The load is too big to carry', time: Date.now()},
    });
    return res.status(412).json({message: 'The load is too big to carry'});
  }

  let truck = null;
  const truckTypeIndex = truckTypes.findIndex((el) => el === suitableTruckType);
  for (let i = truckTypeIndex; i < truckTypes.length; i++) {
    truck = await Truck.findOne({
      assigned_to: {$ne: null},
      status: 'IS',
      type: truckTypes[i],
    });
    if (truck !== null) {
      break;
    }
  }

  if (!truck) {
    await Load.findByIdAndUpdate(id, {
      status: 'NEW',
      logs: {
        message: 'A suitable truck is not available yet',
        time: Date.now(),
      },
    });
    return res.status(200).json({
      message: 'A suitable truck is not available yet',
    });
  }

  await truck.updateOne({status: 'OL'});
  await load.updateOne({
    status: 'ASSIGNED',
    state: 'En route to Pick Up',
    assigned_to: truck.assigned_to,
    $push: {
      logs: {
        message: `Load assigned to driver with id ${truck.assigned_to}`,
        time: Date.now(),
      },
    },
  });
  res.status(200).json({
    message: 'Load posted successfully',
    driver_found: true,
  });
};

const getShippingInfoById = async (req, res) => {
  const id = req.params.id;
  try {
    const load = await Load.findById(id);
    const truck = await Truck.find({assigned_to: load.assigned_to});
    return res.status(200).json({load, truck});
  } catch {
    return res.status(404).json({
      message: `A load with id '${id}' was not found`,
    });
  }
};

module.exports = {
  addLoadForUser,
  getUsersLoads,
  getUsersActiveLoad,
  nextLoadState,
  getUsersLoadById,
  updateUsersLoadById,
  deleteUsersLoadById,
  postUsersLoadById,
  getShippingInfoById,
};
