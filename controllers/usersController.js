const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

const showProfile = (req, res) => {
  res.status(200).json({user: req.user});
};

const changePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findOne({email: req.user.email});
  if (!(await bcrypt.compare(oldPassword, user.password))) {
    res.status(400).json({message: 'Wrong password'});
  }
  await User.findByIdAndUpdate(
      user._id,
      {$set: {password: await bcrypt.hash(newPassword, 10)}},
  );
  res.json({message: 'Password changed successfully'});
};

const deleteProfile = async (req, res) => {
  if (req.user.role === 'DRIVER') {
    return res.status(403).json({
      message: 'You are not allowed to delete your account!',
    });
  }
  await User.findByIdAndDelete(req.user._id);
  res.status(200).json({message: 'Profile deleted successfully'});
};

module.exports = {
  showProfile,
  changePassword,
  deleteProfile,
};
