const {Truck} = require('../models/truckModel');

const postTruck = async (req, res) => {
  const {type} = req.body;
  if (!type) {
    return res.status(400).json({message: `Please specify 'type' parameter`});
  }
  const truck = new Truck({created_by: req.user._id, type});
  await truck.save();
  res.status(200).json({message: 'Truck created successfully'});
};

const getTrucks = async (req, res) => {
  const trucks = await Truck.find({});
  if (trucks.length === 0) {
    res.status(200).json({trucks: []});
  } else {
    res.status(200).json({trucks});
  }
};

const getTruck = async (req, res) => {
  const id = req.params.id;
  try {
    const truck = await Truck.findById(id);
    res.status(200).json({truck});
  } catch {
    res.status(404).json({message: `A truck with id '${id}' was not found`});
  }
};

const putTruck = async (req, res) => {
  const id = req.params.id;
  const {type} = req.body;
  if (!type) {
    return res.status(400).json({message: `Please specify 'type' parameter`});
  }
  try {
    await Truck.findByIdAndUpdate(id, {type});
    res.status(200).json({message: 'Truck details changed successfully'});
  } catch {
    res.status(404).json({message: `A truck with id '${id}' was not found`});
  }
};

const assignTruck = async (req, res) => {
  const id = req.params.id;
  try {
    const assignedTruck = await Truck.find({assigned_to: req.user._id});
    if (assignedTruck.length !== 0) {
      return res.status(412).json({
        message: 'Driver can assign only one truck to himself',
      });
    }
    const truck = await Truck.findById(id);
    if (truck.assigned_to) {
      return res.status(412).json({
        message: `A truck with id '${id}' is already assigned`,
      });
    }
    truck.assigned_to = req.user._id;
    truck.save();
    res.status(200).json({message: 'Truck assigned successfully'});
  } catch {
    res.status(404).json({message: `A truck with id '${id}' was not found`});
  }
};

const deleteTruck = async (req, res) => {
  const id = req.params.id;
  try {
    await Truck.findByIdAndDelete(id);
    res.status(200).json({message: 'Truck deleted successfully'});
  } catch {
    res.status(404).json({message: `A truck with id '${id}' was not found`});
  }
};

module.exports = {
  postTruck,
  getTrucks,
  getTruck,
  putTruck,
  assignTruck,
  deleteTruck,
};
