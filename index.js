const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');

require('dotenv').config();
const {PORT} = require('./config');

app.use(express.json());
app.use(morgan('dev'));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://test-user:qwerty1234@cluster0.dwrzo.mongodb.net/node_hw_3?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}!`);
  });
};

start();
